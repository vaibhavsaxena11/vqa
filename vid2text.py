import tensorflow as tf

class Vid2Text:
    def __init__(self, x, input_shape, embed_size, lr=0.001, scope_name='vid2text'):
        self.x = tf.placeholder(tf.float32, shape=(None, None, input_shape[0], input_shape[1])) # (batch size, timesteps, [input_shape])
        self.input_shape = input_shape
        self.embed_size = embed_size
        self.scope_name = scope_name

        self.lr = lr


    def train_setup(self):
        """ 
        call only once
        all variables get created here
        """
        # l1 = tf.keras.layers.Dense(units) # defines the weights
        # l1(input) # __call__ to Dense object returns: output tensor

        
        with tf.variable_scope(self.scope_name, initializer=tf.constant_initializer(0.01, tf.float32)): # TODO: find better initializer
            # num_layers = 3 # TODO: define in __init__
            # gru_cells = [tf.keras.layers.GRUCell(self.embed_size) for _ in range(num_layers)]
            # stacked_gru_cell = tf.keras.layers.StackedRNNCells(gru_cells) # stacking the GRU cells to behave as one cell
            # output, state = tf.keras.layers.RNN(stacked_gru_cell) # unrolls the rnn (dynamic) # TODO: check

            hl1 = tf.keras.layers.Conv2D(filters=1, kernel_size=5)(self.x)
            gru_ops, gru_states = tf.keras.layers.GRU(self.embed_size, return_sequences=True)(hl1) # use GRUCell if you want to unroll manually later



    
    def _setup_optimizer(self, loss):
        """
        setup optimizer - collect weights, compute grads, apply grads
        returns: apply_grads op
        """
        self.weights = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.scope_name)
        # creating optimizer
        optimizer = tf.train.AdamOptimizer(learning_rate=self.lr, epsilon=1e-04)
        # compute gradients
        grads_and_vars = optimizer.compute_gradients(loss, var_list=self.weights)
        self._grads_and_vars = grads_and_vars ##

        # # clipping gradients by value
        # capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in grads_and_vars]

        # # clipping gradients by global norm
        # grads = [grad for grad, var in grads_and_vars]
        # capped_grads = tf.clip_by_global_norm(grads, 100)[0] # ignored the VRNN/VerifyFinite/control_dependency tensor
        # # TODO: plot gradient norm
        # capped_gvs = [tuple((capped_grads[i], grads_and_vars[i][1])) for i in range(len(grads_and_vars))]
        # self._capped_gvs = capped_gvs ##

        # applying gradients
        apply_grads = optimizer.apply_gradients(grads_and_vars)
        # apply_grads = optimizer.apply_gradients(capped_gvs)


    def fit(self, batch, sess):
        """
        minimize over self.loss by calling sess.run over self.optimizer
        uses batch as input to the placeholder self.x
        """
        sess.run(fetches=self.optimizer, feed_dict={self.x:batch})