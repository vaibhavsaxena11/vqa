#%%
import tensorflow as tf
from vid2text import *
import os
from tqdm import tqdm

# importing Moving MNIST data
data = np.load('../data/mnist_test_seq.npy') # shape (20, 10000, 64, 64)
# data = np.reshape(data, [20, 10000, 4096])
data = data.transpose([1,0,2,3]) # (batch size, timesteps, dims_x, dims_y)
data = data/255 # bring images in 0-255 to 0-1

num_samples = data.shape[0]
batch_size = 10
num_batches = int(num_samples/batch_size)

input_shape = data.shape[2:]
# TODO: bring these from config
latent_size = 16
state_size = 10
embed_size = 10

# x = tf.placeholder(tf.float32, shape=(None, None, input_shape[0], input_shape[1])) # (batch size, timesteps, [input_shape])
model = Vid2Text(input_shape)
model.train_setup()

init_graph = tf.global_variables_initializer()

saver = tf.train.Saver()

#%%
with tf.Session() as sess:
    if os.path.exists("./tmp"):
        saver.restore(sess, "./tmp/model.ckpt")
    else:
        sess.run(init_graph)
    
    # train
    num_epochs = 1
    for epoch in range(num_epochs):
        print('Epoch {}'.format(epoch+1))
        for i in tqdm(range(num_batches)):
            model.fit(data[i*batch_size:(i+1)*batch_size, :, :, :], sess) # calls sess.run

    saver.save(sess, "./tmp/model.ckpt")